#!/usr/bin/env python
## Author: Martijn Derks
## Script to create table with information on the EL candidates

import sys
import gzip
import vcf
import argparse
from pyfaidx import Fasta, wrap_sequence
#from pybedtools import BedTool
#import commands
#import collections

parser = argparse.ArgumentParser(description='Create table with EL candidates')
parser.add_argument("-v", "--vcf_file", help="Compressed VCF file", nargs=1)
parser.add_argument("-o", "--out_file", help="Output tsv file", nargs=1)
parser.add_argument("-f", "--fasta_prot", help="Protein fasta file", nargs=1)

class EL_TABLE:

	def sample_dic(self):
		""" Read sample breed information """	
		self.sample_dic={}
		species_list = open("../../breed_info/final_species_list.tsv","r") 
		header = species_list.readline()
		for sample in species_list:
			samplename, origin, race_species, line_country, pidid = sample.strip().split("\t")
			self.sample_dic[samplename] = [origin.strip(), race_species.strip(), line_country.strip()]

	def parse_vcf_file(self, vcf_file, out_file):
		""" Read VCF and parse deleterious variants """		
		deleterious_list=["stop","start","deleterious","frameshift","splice_donor","splice_acceptor"] ## deleterious variant classes 
		vcf_reader = vcf.Reader(filename=vcf_file, strict_whitespace=True)
		output = open(out_file, 'w') ## Open output file
		output.write("\t".join(["Chr", "Position", "Ref", "Alt", "Type", "Gene", "Hom", "Het", "Amino acid change", "Breed", "SIFT", "Homozygotes", "Heterozygotes"]) + "\n") ## Write header
		for record in vcf_reader: ## Read each VCF record
			if len(record.ALT) == 1 and int(record.QUAL) > 30: ## Ignore low quality variants
				for csq in record.INFO['CSQ']: ## Read VEP CSQ tag
					for type in deleterious_list: ## Only select variants considered to be deleterious
						if type in csq:
							chrom, pos, var_id, ref, alt = record.CHROM, record.POS, record.ID, record.REF, record.ALT[0] ## Get variant information
							ac_hom, ac_het = record.num_hom_alt, record.num_het
							if ac_hom <= 1 and ac_het >= 3 and ac_het < 10:	## Select low frequency variants with maximum one homozygous sample observed		
								breed_dic={}
								het_samples, hom_samples = [],[]
								sift, provean_score, gene,breed,mgi_id  = "", "-", "", "",""
								csq=csq.split("|")
								type, symbol, geneid, transcript = csq[1], csq[3], csq[4], csq[6]
								aa_change = csq[14].split("-")[0]+"-"+csq[15]	
								for het in record.get_hets():
									het_samples.append(het.sample)
									breed = "-".join(self.sample_dic[het.sample][1:])
									if breed in breed_dic:
										breed_dic[breed] +=1
									else:
										breed_dic[breed] = 1				
								for hom in record.get_hom_alts():
									hom_samples.append(hom.sample)
									breed = "-".join(self.sample_dic[hom.sample][1:])
									if breed in breed_dic:
											breed_dic[breed] +=1
									else:
											breed_dic[breed] = 1
								breed = ", ".join(["=".join([key, str(val)]) for key, val in breed_dic.items()])
								if symbol == "":
									symbol = geneid
								hom_samples = ",".join(hom_samples)
								if hom_samples == "":
									hom_samples = "-"
								het_samples = ",".join(het_samples)
								if "deleterious" in "".join(csq):
									try:
										sift = csq[24].split('(')[1].split(')')[0]
									except:
										sift = csq[25].split('(')[1].split(')')[0]
								else:
									sift="-"
								output.write("\t".join(map(str, [chrom, pos, ref, alt, type, symbol, ac_hom, ac_het, aa_change, breed, sift, hom_samples, het_samples])) + "\n") ## Write output
						
if __name__== "__main__":
	args = parser.parse_args()
	vcf_file = args.vcf_file[0]
	output = args.out_file[0]
	fasta_protein_file = args.fasta_prot[0]

	E = EL_TABLE()
	E.sample_dic()
	E.parse_vcf_file(vcf_file, output)
