# -*- coding: utf-8 -*-
"""
Created on Wed Feb 13 10:49:46 2019

@author: cgross
This script reads in the coordinates and of query locations within the autosomes of Pig and X and returns alls RAW and PHRED-scores for those positions.
"""

import os,sys
import pysam
import argparse


parser = argparse.ArgumentParser(description='')
parser.add_argument("-o", "--outfile", dest="outfile", help="If defined, writes the output into specified file and gives it a header line, otherwise writes to stdout without header. The output consists of 6 tab delimited values. The chromosome, position, reference nucleotide, alternative nucleotide, the Raw score and the PHRED score for the queried regions. default = \"\"", default='')
parser.add_argument("-c", "--chr", dest="chr", help="The chromosome label, accepted are labels such as \"chr1\" or \"1\". default = \"\"", default='')
parser.add_argument("-s", "--start", dest="start", help="The 5' start position of the region for which you want to retrieve the PHRED-scores. Coordinates are considered to be 1-based", type=int, default=0)
parser.add_argument("-e", "--end", dest="end", help="The 5' end position of the region for which you want to retrieve the PHRED-scores. Coordinates are considered to be 1-based", type=int, default=0)
parser.add_argument("-P","--PHRED", dest="PHRED", help="The path to the pCADD-PHRED file. It has to be bgzipped and tabix indexed.", default = "sort_pCADD-RAW-PHRED-scores.tsv.gz")
args = parser.parse_args()


#Checking if input parameters are correct.
if not os.path.isfile(args.PHRED):
    sys.exit("pCADD-PHRED file could not be found under following this path:\n %s"%args.PHRED)

if args.chr.startswith('chr'):
    args.chr = args.chr[3:]
if args.chr not in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','X']:
    sys.exit("The Chromosome could not be found, it has to be one of the autosomes 1-18 or X. Current chromosome considered:\n %s, chr%s"%(args.chr,args.chr))

if not args.start<=args.end:
    sys.exit("The Start position has to be equal to the End position or smaller. The parsed Start and End positions are following:\n Start:%i End:%i"%(args.start,args.end))
if (args.start<=0):
    sys.exit("The Start position has to be larger than 0")

if args.outfile=='':
    outstream = sys.stdout
else:
    outstream = open(args.outfile,'w')
    outstream.write("#Chrom\tPos\tRef\tAlt\tRAW\tPHRED\n")

#iterating over query
phred_file = pysam.Tabixfile(args.PHRED)
for record in phred_file.fetch(args.chr,args.start-1,args.end):
    print >> outstream, record
