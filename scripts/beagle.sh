#!/bin/bash
#SBATCH --time=1-0:0:0
#SBATCH -n 1
#SBATCH -c 8
#SBATCH --error=error_beagle_%j.txt
#SBATCH --job-name=AG_beagle_%j
#SBATCH --partition=ABGC_Std
#SBATCH --exclude=fat001,fat002
#SBATCH --mem=12000

module load java/jre/1.8.0/144

java -Xmx12g -jar ../../beagle/beagle.03Jul18.40b.jar \
gt=$1 \
out=Sscrofa.chr6.phased \
window=30 \
overlap=4 \
ne=100 \
nthreads=8

