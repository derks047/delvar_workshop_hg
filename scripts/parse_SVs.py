import sys
import vcf

WAfile=sys.argv[1]

def get_vcf_information(file):
	vcf_reader = vcf.Reader(filename=file, strict_whitespace=True)
	print "Chr\tPos\tHeterozygotes\tHomozygotes\tHet_samples\tHom_samples\tSVtype\tLength\tEnd\tCSQ\tDepth\tAF"
	for record in vcf_reader:
		het_samples,hom_samples=[],[]
		for sample in record.get_hets():
				het_samples.append(sample.sample)
		for sample in record.get_hom_alts():
				hom_samples.append(sample.sample)
		if het_samples == []:
				het_samples = ["-"]
		if hom_samples == []:
				hom_samples = ["-"]
		outlist= [record.CHROM,record.POS,record.num_het, record.num_hom_alt, ",".join(het_samples), ",".join(hom_samples), record.INFO['SVTYPE'], record.INFO['SVLEN'][0], record.INFO['END'], "".join(record.INFO['CSQ']), ",".join(record.INFO['DEPTH']),round((record.num_het+record.num_hom_alt*2)/float((71*2)),4)]
		print "\t".join(map(str,outlist))
		
get_vcf_information(WAfile)