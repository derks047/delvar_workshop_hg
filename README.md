# DelVar_Workshop

Code and tutorials for the DelVar workshop.

Clone the repository in your working directory:

git clone --recursive https://git.wur.nl/derks047/delvar_workshop_hg.git  

In this workshop we will focus on the following topics:
	
	1. Working with VCF files.
	2. Calling deleterious variation from whole-genome-sequence in chicken.
	3. Map recessive lethal haplotypes in pigs.
	4. Identify the causal mutation.
	5. Runs of homozygosity analysis in one turkey breed.
	6. Structural variation analysis in chicken

### Part 1: Working with VCF files ###
------------------

Navigate to the data/vcf directory

~~~~ {.bash}
cd delvar_workshop_hg/data/vcf
~~~~

Make sure python2, bcftools, plink and java can be found:

~~~~ {.bash}
module load python/2.7.15
module load bcftools/gcc/64/1.2
module load plink/1.9-180913
module load java/jre/1.8.0/144
~~~~

VCF files can get very big. Being able to manipulate them and extract
relevant information is important. In practice, flat text files such as
VCF are currently the basis for further analysis. 

Note, that you can always recode plink files into VCF format using the "recode vcf-iid" option.
Example:

~~~~ {.bash}
plink2 --file test --recode vcf-iid 
~~~~

VCF files provide an easy to parse format for both genotype and WGS data. Look at a subset VCF file from chromosome 1:

~~~~ {.bash}
bcftools view Chr1_subset.vcf.gz | grep -v "##" | less
~~~~

The file includes 250 white-layer samples (WA,WD,W1). The first columns provide information on chromosome, position, reference allele, and alternative allele.

The INFO field provides population information (e.g. allele frequency, number of homozygous and heterozygous samples.)

The CSQ tag provides the effect prediction from the allele (coding/intron/intergenic). We will look at this later in the tutorial. 

What is the allele frequency for the first variant in the file?

For more information of the basic features of the VCF. 

` `[`http://www.1000genomes.org/wiki/analysis/vcf4.0`](http://www.1000genomes.org/wiki/analysis/vcf4.0)

### Part 2: Calling deleterious variation from whole-genome-sequence in chicken ###

We can examine whole genome sequence data (WGS) for possible deleterious variants. 
WGS data consists of reads (forward and reverse) that are aligned to the chicken reference genome (Gal6) using the software BWA-MEM.
The output alignment files are stored in a BAM file (Binary Alignment Map). 
There are many tools to perform variant calling on this BAM alignment file (e.g.  GATK, Mpileup, freebayes).

To get an idea of this process, we will perform variant calling on 1 chicken sample from the WD breed (Chicken1.bam file). 
This file contains the alignments of 1 chicken in the region 1:0-30Mb.
The header of the BAM file contains the information on the reference sequence and the settings that were used for the mapping. 

First navigate to the wgs folder and load samtools

~~~~ {.bash}
module load samtools/gcc/64/1.5
cd ../wgs/
samtools view -H Chicken1.bam | less ## examine header
samtools view Chicken1.bam | less ## Examine alignments
~~~~

Each row in the alignment file represents a read that was mapped to chromosome 1:0-30 Mb.

Next we will perform variant calling using freebayes (this can take a minute or 2):

~~~~ {.bash}
module load freebayes/gcc/64/1.0.2
freebayes --bam Chicken1.bam -f ../../reference/Gallus_gallus.GRCg6a.dna.toplevel.fa --min-base-quality 20 --min-mapping-quality 30 --min-alternate-fraction 0.2 --haplotype-length 0 --min-alternate-count 3 | bgzip -c > Chicken1.vcf.gz
~~~~

Subsequently you can use various tools to make more precise annotations on the variant effects (e.g. protein altering). Among them are Variant Effect
Predictor (VEP), snpEff, and Annovar. We will use the variant effect predictor to annotate the variants.

VEP can be found here:

~~~~ {.bash}
/cm/shared/apps/SHARED/ensembl-vep/vep --help
~~~~

If you invoke it without parameters you will see the following:

` Usage:`\
` ./vep [--cache|--offline|--database] [arguments]`\
` `\
` Basic options`\
` =============`\
` `\
` --help                 Display this message and quit`\
` `\
` -i | --input_file      Input file`\
` -o | --output_file     Output file`\
` --force_overwrite      Force overwriting of output file`\
` --species [species]    Species to use [default: "human"]`\
`                        `\
` --everything           Shortcut switch to turn on commonly used options. See web`\
`                        documentation for details [default: off]                       `\
` --fork [num_forks]     Use forking to improve script runtime`\
` `\
` For full option documentation see:`\
` `[`http://www.ensembl.org/info/docs/tools/vep/script/vep_options.html`](http://www.ensembl.org/info/docs/tools/vep/script/vep_options.html)\
` `

We used the following options to run VEP on our VCF file of variants:

` --species gallus_gallus`\
` --vcf`\
` --sift b`\
` --offline   # using a cache directory`\
` --dir /lustre/nobackup/SHARED/cache/ # where the cache dir lives`\

~~~~ {.bash}
/cm/shared/apps/SHARED/ensembl-vep/vep -i Chicken1.vcf.gz --offline --dir /lustre/nobackup/SHARED/cache/ --species gallus_gallus --vcf --sift b -o Chicken1.vep.vcf
~~~~

You can now explore the variant annotation in the VCF file, and grep for deleterious variants.

Show consequences VEP

~~~~ {.bash}
less Chicken1.vep.vcf ## Have a look at the CSQ field in the INFO tags.
grep "deleterious" Chicken1.vep.vcf | less
~~~~

How many deleterious missense variants do you find?

Moreover, we can look at the VEP reports by looking at the html file in a browser.
Alternatively you can copy the html file to your local computer and open in a webbrowser of choice.

~~~~ {.bash}
firefox Chicken1.vep.vcf_summary.html
~~~~

Which type of variants are most common? 

The VEP output you generated before has many advantages, one of which is
that it is nicely tabular and easy to parse. 

From studying the HTML reports it should be clear that the information
you can find there is necessarily limited and general. It is therefore
important to acquire skills to generate further analyses based on the
annotation that meet your specific research needs. In this section we
will delve a little bit deeper in the structure of the annotations and
how to extract information from it.

Although the VCF now looks even more complex than before, there is a
good reason to add the VEP annotation data the way it was done. The 8th
column of the VCF (the "INFO" column) provides a lot of information,
each field separated by a ';'. In addition, each field contains a field
name and a value, delimited by a '='. The VEP annotation has its own
field name, or tag: 'CSQ'. Within this field, the values are again
structured, but this time delimited by a '|'. What we in fact have now
is a nested data structure:

1       23423617        .       T       C       856.796 .       AB=0;ABP=0;AC=2;AF=1;AN=2;AO=17;CIGAR=1X;DP=17;DPB=17;DPRA=0;EPP=3.13803;EPPR=0;GTI=0;LEN=1;MEANALT=1;MQM=60;MQMR=0;NS=1;NUMALT=1;ODDS=28.1722;PAIRED=1;PAIREDR=0;PAO=0;PQA=0;PQR=0;PRO=0;QA=1092;QR=0;RO=0;RPL=7;RPP=4.1599;RPPR=0;RPR=10;RUN=1;SAF=7;SAP=4.1599;SAR=10;SRF=0;SRP=0;SRR=0;TYPE=snp;technology.ILLUMINA=1;CSQ=C|missense_variant|MODERATE|CPED1|ENSGALG00000009002|Transcript|ENSGALT00000014649|protein_coding|21/23||||3003|2787|929|I/M|atA/atG|||-1||HGNC|HGNC:26159|deleterious(0)

Most genes have zero or one deleterious variant but there is also a number of genes 
that has quite a few deleterious alleles annotated in the current population sample. 
Say you would like to make a table that contains all genes that have more than one putative
deleterious variant. We would need to isolate the
CSQ field, and from the latter isolate the 'gene' field. While this is
possible with a fairly simple shell scripting hack, it does require an
awful lot of counting to make sure you end up with the right fields.

Lets count the number of deleterious variants per gene using this one-liner:
~~~~ {.bash}
grep "deleterious" Chicken1.vep.vcf | cut -f8 | cut -d';' -f43 | cut -d'|' -f5 | sort | uniq -c | sort -n -k1
~~~~

Which two genes have > 8 deleterious variants annotated? 

Look up the function of these two genes in Ensembl, what are the functions of these genes (hint: look at orthology)? 

Also look at the function of a gene with only 1 deleterious variant annotated? and look at the orthology? What do you notice?

### Part 3: Map recessive lethal haplotypes in pigs. ###

Go to the phasing folder in the data directory:

~~~~ {.bash}
cd ../phasing
~~~~

In order to identify haplotypes we need to phase the data.
We prepared 50K genotypes from one pig breed (part of chromosome 6).
Have a look at what is inside this file:

Header:
~~~~ {.bash}
bcftools view -h Sscrofa.chr6.vcf.gz | less ## Examine header 
~~~~

or

Genotypes:
~~~~ {.bash}
bcftools view Sscrofa.chr6.vcf.gz | less ## Examine genotypes
~~~~

### Phasing the VCF file.

In order to call haplotypes from this VCF file we phase the data using Beagle software. 

Run the following in your phasing directory. Make sure you changed the partition to Hendrix-Genetics in the 'beagle.sh' shell script.

~~~~ {.bash}
sbatch ../../scripts/beagle.sh Sscrofa.chr6.vcf.gz
~~~~

Beagle will phase chr6 from 30 - 60 Mb with an Ne (effective population size) of 100.

The phasing can take a few minutes...

Now lets explore the result file:

~~~~ {.bash}
bcftools view Sscrofa.chr6.phased.vcf.gz
~~~~

You will see that the data is now phased (see the pipes dividing the genotypes).

### Detecting haplotypes exhibiting missing homozygosity ###

In order to find haplotypes that show a deficit in homozygosity we wrote a python script to analyse the phased genotypes. 
You can run the help function to see what the pipeline requires as input (ignore possible warnings).

~~~~ {.bash}
python ../../scripts/pipeline_HHD_basic.py --help
~~~~

usage: pipeline_HHD_basic.py [-h] [-v VCF_FILE] [-p PEDIGREE] [-o OUTPUT_FILE]
                             [-w WINDOW_SIZE]

Analyse missing homozygote haplotypes to identify lethal recessives using
phased population data

optional arguments: \
  -h, --help            show this help message and exit \
  -v VCF_FILE, --vcf_file VCF_FILE \
                        phased VCF_file (compressed) \
  -p PEDIGREE, --pedigree PEDIGREE \
                        Pedigree (pig_id sire dam), space delimited \
  -o OUTPUT_FILE, --output_file OUTPUT_FILE \
                        Output file \
  -w WINDOW_SIZE, --window_size WINDOW_SIZE \
                        Window size to build haplotypes (bp) \

The pipeline requires a phased VCF, a pedigree file, an output filename, and a scanning window size.

Now lets run the pipeline using a scanning window size of 1.5 Mb.

~~~~ {.bash}
python ../../scripts/pipeline_HHD_basic.py -v Sscrofa.chr6.phased.vcf.gz -p ../../pedigree/pedigree.csv -o outfile.tsv -w 1500000
~~~~

Look at the output of the outfile.tsv in e.g. excel. You'll see that there is one haplotype showing missing homozygosity, the haplotype consists of 34 markers, the frequency is 4.8% (9.6% carrier frequency), 
expected homozygotes is ~36 (based on frequency) while 0 observed. 

This haplotype therefore likely harbours a lethal allele, causing the lack of homozygotes. 

Which whole genome sequenced samples (WGS) are carrier of the "lethal" haplotype?

### Part 4: Identify the causal mutation ###

As mentioned above, there are 5 carrier animals from the lethal haplotype that are sequenced (WGS). 
Lets explore this sequence data to find potential causal mutations.
There is a vcf file with all the coding variants in the region 42-52 Mb on chromosome 6 in the examined pig breed. 
This vcf file was created using freebayes (https://github.com/ekg/freebayes).
Although freebayes is a highly efficient tool for variant calling we will skip this part due to time constraints.

Now lets try and find candidate causal mutations for the lethal haplotype on chromosome 6.
An in-house python script will parse all low-frequency (AF <0.2) deleterious variants and output in tabular format. 

~~~~ {.bash}
python ../../scripts/table_EL.py -v coding.Sscrofa.chr6.vep.vcf.gz -o Sscrofa_deleterious.tsv -f Sus_scrofa.Sscrofa11.1.pep.all.fa
~~~~

Inspect the output file in excel, how many variants do you find?

You will see there are three variants that are carried by the recessive lethal haplotype carriers (Pig_277,Pig_283,Pig_WUR_398,Pig_WUR_403,Pig_WUR_418).
Two frameshift variants (SPTBN4, ZNF404) and one splice_acceptor (ENSSSCG00000039128). 
The ENSSSCG00000039128 gene is of unknown function, and we cannot infer any functional consequence on this gene.

One way to check the importance of a gene is to look how intolerant a gene is to loss-of-funtion mutations.
Visit following website: http://genic-intolerance.org/ and read the "About" page. What does the RVIS score represent?

Now look up both genes, which genes is most intolerant to LoF mutations? Could this gene be subject to recessive lethal mutations?

### Inspect the variants in a browser ###

The HPC hosts a JBrowse instance, where you can inspect alignments and variants of all sequenced individuals: https://jbrowse.anunna.wur.nl/
Navigate to the Pig 11.1 browser: https://jbrowse.anunna.wur.nl/pig_sscrofa11.1/

Look up the position of the SPTBN4 frameshift mutation and
select two carrier animals in the 2. In-house ReSeq alignments (e.g.: Pig_277, Pig_WUR_418): https://jbrowse.anunna.wur.nl/pig_sscrofa11.1/?loc=6%3A48801248..48801319&tracks=DNA%2CEnsembl_Genes%2CEnsembl_Transcripts%2CPig_277_rh.dedup_st.reA_alignment%2CPig_WUR_418_alignment&highlight= 

Do you think this is a true frameshift mutation? Do the same for the ZNF404 mutation. 

Look at the pCADD scores in the frameshift regions? What do you notice about the pCADD scores in general in coding sequences?

https://jbrowse.anunna.wur.nl/pig_sscrofa11.1/?loc=6%3A48801255..48801326&tracks=DNA%2CEnsembl_Genes%2CEnsembl_Transcripts%2CPig_277_rh.dedup_st.reA_coverage%2C6_max-PHRED-pCADD&highlight=

Christian made a script to query pCADD scores in specific regions, look up the pCADD scores in the frameshift region:

~~~~ {.bash}
pip install pysam --user ## install pysam package
python ../../scripts/retrieve_pCADD_scores.py -o pCADD.tsv -c 6 -s 48801281 -e 48801296 -P sort_pCADD-RAW-PHRED-scores.tsv.gz 
~~~~

Look at the 2 variants with highest pCADD scores? Why do you think these scores are the highest? (tip: look at the codons in Jbrowse).

Which mutation if the most likely candidate to cause lethality? 
Look up both genes in e.g. OMIM database, or MGI database to see the phenotypes of the knockouts.

What do you think these piglets will suffer from?

### Part 5. Runs of homozygosity analysis in Turkeys ###

Runs of homozygosity (ROH) can be used as a measure of genomic inbreeding. 
In this tutorial we will perform ROH analysis using plink and detectRUNS of 200 random samples of one turkey breed (TG.ped).
First navigate to the data/ROH directory.

~~~~ {.bash}
cd ../ROH/ ## Navigate to ROH directory.
~~~~

Lets start by running ROH analysis in plink:

~~~~ {.bash}
plink --homozyg group \
	--homozyg-density 50 \ # at least one SNP per 50 kb
	--homozyg-window-het 1 \ ## Het. SNPs allowed in ROH
	--homozyg-kb 10 \ ## min length ROH
	--homozyg-window-snp 30 \ ## SNPs in scanning window contains 30 SNPs
	--allow-no-sex \
	--out MgalROH \
	--chr-set 33 \
	--file TG
~~~~

Or use this one-liner: 

~~~~ {.bash}
plink --homozyg group --homozyg-density 50 --homozyg-window-het 1 --homozyg-kb 10 --homozyg-window-snp 30 --allow-no-sex --out MgalROH --chr-set 33 --file TG
~~~~

The file "MgalROH.hom.indiv" contains the sum of all ROHs per individual (column: KB) and the average length (KBAVG).
First we need to convert the output to tab delimited format:

~~~~ {.bash}
awk '{print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6}' MgalROH.hom.indiv > MgalROH.hom.indiv.tsv
~~~~

Now we can have a look at the proportion of the genome that is homozygous (Froh). We can plot this using the following python script:

~~~~ {.bash}
python ../../scripts/ROH_violin_KB.py MgalROH.hom.indiv.tsv
~~~~

What is the average Froh in this population (Turkey_Froh.pdf)? 
Moreover, the average length of the ROHs can provide information of how recent the inbreeding is.
More recent inbreeding usually results in longer ROHs, while "older" inbreeding leads to shorter ROHs.
You can look at the average ROH length in the following file: MgalROH_AVG_Length.pdf

## detectRUNS ##

Now lets use another tool to predict ROHS. 
We are going to use the R package detectRUNS (https://cran.r-project.org/web/packages/detectRUNS/vignettes/detectRUNS.vignette.html)

Runs the script detectRUNS in the ROH data directory

~~~~ {.bash}
module load R/3.5.0
Rscript ../../scripts/detectRUNS.Rscript
~~~~

Inspect the output file: detectRUNS_TG.pdf

What is the most inbred chromosome in this breed (look at the Froh per chromosome)? And what is the least inbred chromosome?

Can you identify possible sweep regions from the ROH output?

Look at the sweep on Chr4 (~50 Mb). It comprises the TBC1D1 gene. A well known gene involved in growth. 

Look up some information about this gene in chicken, or check the gene in the MGI database (mouse knockout). Do you think this gene is under selection in Turkeys?

## Part 6. Structural variation in chicken ##

Various tools can be used to analyse structural variation from sequence data. In this project we used the Smoove pipeline to detect structural variation.
Information about the pipeline and how to run the pipeline can be found here: https://git.wageningenur.nl/derks047/Smoove_SV

In short, smoove uses discordant and split reads to identify SVs. Also coverage information is used as an additional layer of evidence.  

In this tutorial we will look at the output file of deletions and duplications in the WA breed. 
Sort the output deletions on the length and look at the second largest deletion (SVLEN) 

~~~~ {.bash}
cd ../SVs ## navigate to SVs folder
zcat Duplications.vcf.gz | grep -v "#" | grep "coding_sequence" | cut -f8 | cut -d';' -f2 | sort -r -V | head -2 | zgrep -F -f - Duplications.vcf.gz ## one-lines to get largest duplications
~~~~

What is the size of this deletion (look at the SVLEN tag)? On which chromosome is it located? Which gene(s) are affected?

Be aware that there are also more elegant ways to sort on INFO field in the VCF: http://lindenb.github.io/jvarkit/SortVcfOnInfo.html

Now lets parse the VCF output in tab delimited format. We can use an in-house python script for this:

~~~~ {.bash}
pip install PyVCF --user ## install pyvcf package
python ../../scripts/parse_SVs.py Duplications.vcf.gz > WA_Duplications.tsv
~~~~

Open the output file in excel and sort on the allele frequency (AF).
One variant affecting the ELMOD1 gene is nearly fixed in the population, look at the variant in a browser:

https://jbrowse.anunna.wur.nl/chicken/?loc=1%3A180531444..180636243&tracks=DNA%2CGenes%2CTranscripts%2CGG-WUR-113_rh.dedup_st.reA_coverage&highlight= 

What is the orientation of both genes that are likely affected by the duplication? Does it likely affect the expression of the ELMOD1 gene?

Now have a look at the deletions:

~~~~ {.bash}
python ../../scripts/parse_SVs.py Deletions.vcf.gz > WA_Deletions.tsv
~~~~

Look up the deletion affecting the CLIP1 gene, do you expect this deletion to be deleterious in homozygous state?

https://jbrowse.anunna.wur.nl/chicken/?loc=15%3A5853907..5922006&tracks=DNA%2CGenes%2CTranscripts%2CGG-WUR-123_rh.dedup_st.reA_coverage&highlight=

### End of tutorial ###
